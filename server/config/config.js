// Puerto
process.env.PORT = process.env.PORT || 3000;
// Entorno
process.env.NODE_ENV = process.env.NODE_ENV || 'dev'
// Database
process.env.DATABASE = process.env.NODE_ENV === 'dev' ? 
'mongodb://localhost:27017/cafe':
'mongodb+srv://dev:devpass@cluster0-pfvjo.mongodb.net/cafe';
// Vencimiento del token
process.env.CADUCIDAD_TOKEN = '30 days';
// SEED token
process.env.SEED = process.env.SEED || 'randomseed';