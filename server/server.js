require('./config/config');

const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser')

const app = express();

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
app.use(bodyParser.json())
// user routes
app.use(require('./routes'));


mongoose.connect(process.env.DATABASE, {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useCreateIndex: true,
	useFindAndModify: false,
}, (err) => {
	
	if (err) throw err;
	
	console.log('Base de datos ONLINE: ', process.env.DATABASE);
});

app.listen(process.env.PORT, () => {
	console.log('Listen on port: ', process.env.PORT);
});