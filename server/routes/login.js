const express = require('express');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const app = express();

const Usuario = require('../models/usuario');

app.post('/login', (req, res) => {

	const body = req.body;

	console.log(req);

	Usuario.findOne({
		email: body.email,
	}, (err, usuario) => {
		
		if (err) {
			return res.status(500).json({
				ok: false,
				err,
			});
		}

		if (!usuario) {
			return res.status(400).json({
				ok: false,
				err: {
					message: 'Usuario o contraseña incorrectos'
				},
			});
		}

		// if (!bcrypt.compareSync(body.password, usuario.password)) {
		if (body.password !== usuario.password) {
			return res.status(400).json({
				ok: false,
				err: {
					message: 'Usuario o contraseña incorrectos'
				},
			});
		}

		const token = jwt.sign({
			usuario,
		}, process.env.SEED, { expiresIn: process.env.CADUCIDAD_TOKEN });

		res.json({
			ok: true,
			usuario,
			token,
		})
	});	
});

module.exports = app;