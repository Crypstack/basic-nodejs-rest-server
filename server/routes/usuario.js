const express = require('express');
const bcrypt = require('bcrypt');
const _ = require('underscore');

const app = express();

const Usuario = require('../models/usuario');
const { verificaToken, verificaRole } = require('../middlewares/auth');

app.get('/usuario', verificaToken, function (req, res) {
	
	const skip = Number(req.query.skip) || 0;
	const take = Number(req.query.take) || 5;

	Usuario.find({ estado: true }, 'nombre email')
		.skip(skip)
		.limit(take)
		.exec((err, usuarios) => {
			if (err) {
				return res.status(400).json({
					ok: false,
					err,
				});
			}

			Usuario.countDocuments({ estado: true }, (err, count) => {
				res.json({
					ok: true,
					usuarios,
					count,
				})
			});

		});


});

// app.post('/usuario', [verificaToken, verificaRole], function (req, res) {
app.post('/usuario', function (req, res) {
	const body = req.body;

	let usuario = new Usuario({
		// desctructurar no es completamente seguro ya que pueden
		// mandar cualquier cosa desde el front y esta se guardará
		// en la base de datos
		nombre: body.nombre,
		email: body.email,
		// password: bcrypt.hashSync(body.password, 10),
		password: body.password,
		role: body.role,
	});

	usuario.save((err, usuarioDB) => {

		if (err) {
			return res.status(400).json({
				ok: false,
				err,
			});
		}

		res.json({
			ok: true,
			usuario: usuarioDB
		});

	});
});

app.put('/usuario/:id', [verificaToken, verificaRole], function (req, res) {
	let id = req.params.id;
	let body = _.pick(req.body, ['nombre', 'email', 'img', 'role', 'estado']);

	Usuario.findByIdAndUpdate(id, body, {new: true, runValidators: true, context: 'query'}, (err, doc) => {
		if (err) {
			return res.status(400).json({
				ok: false,
				err,
			});
		}

		res.json({
			ok: true,
			usuario: doc,
		});
	});
});

app.delete('/usuario/:id', [verificaToken, verificaRole], function (req, res) {
	let id = req.params.id;
	
	Usuario.findByIdAndUpdate(id, { estado: false }, { new: true }, (err, doc) => {
		if (err) {
			return res.status(400).json({
				ok: false,
				err,
			});
		}
		
		if (!doc) {
			return res.status(400).json({
				ok: false,
				error: {
					message: 'usuario no encontrado',
				},
			});
		}

		res.json({
			ok: true,
			usuario: doc,
		});
	});

});

module.exports = app;