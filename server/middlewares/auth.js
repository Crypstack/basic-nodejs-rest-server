const jwt = require('jsonwebtoken');

// Verificar token
const verificaToken = (req, res, next) => {
	
	const token = req.get('token');

	jwt.verify(token, process.env.SEED, (err, decoded) => {

		if (err) {
			return res.status(401).json({
				ok: false,
				err: {
					message: 'Token inválido',
				},
			});
		}

		req.usuario = decoded.usuario;
		next();

	});
};

// Verificar Admin role
const verificaRole = (req, res, next) => {

	const usuario = req.usuario;

	if (usuario.role === 'ADMIN_ROLE') {
		next();
	}
	else {
		return res.json({
			ok: false,
			err: {
				message: 'El usuario no es ADMIN',
			}
		});
	}

};



module.exports = {
	verificaToken,
	verificaRole,
};